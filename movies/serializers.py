from rest_framework import serializers
from movies.models import Movies

class MovieListSerializer(serializers.ModelSerializer):
    class Meta:
        model = Movies
        fields = ['id','name','director','popularity','imdb_score','genre','user_id']

class MovieCreateSerializer(serializers.ModelSerializer):
    class Meta:
        model = Movies
        fields = ['name','director','popularity','imdb_score','genre']

    def create(self,validated_data):
        validated_data['user_id'] = self.context['request'].user
        return super(MovieCreateSerializer, self).create(validated_data)

class MovieUpdateSerializer(serializers.ModelSerializer):
    class Meta:
        model = Movies
        fields = ['name','director','popularity','imdb_score','genre']

class MovieDeleteSerializer(serializers.ModelSerializer):
    class Meta:
        model = Movies
        fields = ['name', 'director', 'popularity', 'imdb_score', 'genre']
