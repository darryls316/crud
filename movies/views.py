from movies.permission import IsAdmin,IsUser
from rest_framework.generics import (
    ListAPIView,
    CreateAPIView,
    UpdateAPIView,
    DestroyAPIView,
)
from rest_framework.permissions import IsAuthenticated
from movies.serializers import MovieListSerializer,MovieCreateSerializer,MovieUpdateSerializer,MovieDeleteSerializer
from django.http.response import HttpResponse
from movies.models import User,Movies
from django.contrib.auth.hashers import make_password


# Create your views here.

def create_user(request):
    username = request.POST.get("username")
    password = request.POST.get("password")
    hashpassword = make_password(password)
    role = request.POST.get("role")

    User.objects.create(username=username, password=hashpassword, role=role)
    return HttpResponse("User created!")

class CreateMovie(CreateAPIView):
    permission_classes = [IsAuthenticated,IsAdmin]
    serializer_class = MovieCreateSerializer

class UpdateMovie(UpdateAPIView):
    permission_classes = [IsAuthenticated,IsAdmin]
    queryset = Movies.objects.all()
    lookup_field = 'id'
    serializer_class = MovieUpdateSerializer

class DeleteMovie(DestroyAPIView):
    permission_classes = [IsAuthenticated,IsAdmin]
    queryset = Movies.objects.all()
    lookup_field = 'id'
    serializer_class = MovieDeleteSerializer

class MovieList(ListAPIView):
    permission_classes = [IsAuthenticated,IsUser]
    serializer_class = MovieListSerializer
    queryset = Movies.objects.all()
