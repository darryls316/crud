from django.db import models
from django.contrib.auth.models import AbstractUser

# Create your models here.

class User(AbstractUser):
    role = models.CharField(db_column='role', max_length=50)
    dtCreated = models.DateTimeField(auto_now_add=True)

class Movies(models.Model):
    id = models.BigAutoField(db_column='id', primary_key=True)
    name = models.CharField(db_column='name', max_length=50)
    director = models.CharField(db_column='director', max_length=50)
    popularity = models.FloatField(db_column='popularity')
    imdb_score = models.FloatField(db_column='imdb_score')
    genre = models.CharField(db_column='genre', max_length=50)
    user_id = models.ForeignKey(User)