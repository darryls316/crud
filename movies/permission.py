from rest_framework.permissions import BasePermission

class IsAdmin(BasePermission):
    def has_permission(self, *args,**kwargs):
        request = args[0]
        if request.user.role == "admin":
            return True
        return False

class IsUser(BasePermission): 
    def has_permission(self, *args,**kwargs):
        request = args[0]
        if request.user.role == "user":
            return True
        return False
